
package kata;

/**
 * Calculates the total charge for a night of babysitting
 * @author Wade
 */
public class Babysitter {
    public int totalCharge(int startTime, int endTime, int bed){
        int payout = 0;
        if (endTime < 17)
            endTime = endTime + 24;
        if (startTime < 17)
            startTime = startTime + 24;
        for (int i = startTime; i < endTime; i++){
            if (i >= 24)
                payout = payout + 16;
            else if (i >= 17 && i < bed)
                payout = payout + 12;
            else 
                payout = payout + 8;
            System.out.println(payout);
        }
        return payout;
    }
}
