
package kata;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Babysitter kata unit framework
 * @author Wade
 */
public class BabysitterTest {
    private Babysitter babysitter;
    
    public BabysitterTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        babysitter = new Babysitter();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void allowsTimesBetween5pmAnd4am() {
        assertEquals(24, babysitter.totalCharge(18, 20, 22));
    }
    
    @Test
    public void confirmHourlyRatesAreCorrect() {
        assertEquals(12, babysitter.totalCharge(17, 18, 20));
        assertEquals(8, babysitter.totalCharge(20, 21, 20));
        assertEquals(16, babysitter.totalCharge(1, 2, 21));
    }
    
    @Test
    public void payVariesBasedOnHour() {
        assertEquals(64, babysitter.totalCharge(17, 23, 21));
        assertEquals(32, babysitter.totalCharge(22, 1, 20));
    }
    
    @Test
    public void confirmDifferentBedtimeAdjustsPay() {
        assertEquals(132, babysitter.totalCharge(17, 4, 20));
        assertEquals(144, babysitter.totalCharge(17, 4, 23));
    }
    
}
